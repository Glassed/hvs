/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileFinderTest;

import configuration.Config;
import gq.kiev.homevideoserver.model.VideoModel;
import gq.kiev.homevideoserver.service.filesystem.FileFinderService;
import java.util.List;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Config.class)
public class FileFinderTest {
        
    @Autowired
    FileFinderService finder;
    
    
    
    //private String[] ext = {".mp4"};
    
    
    @Test
    public void findFiles() {
        //finder.setFileExtension(ext);
        finder.refleshFileHolder();
        
        
        
        List<VideoModel> list = finder.getVideoModels();
        for (VideoModel video : list) {
            System.out.println("******");
            System.out.println(video);
            System.out.println("******");
        }
        
        assertThat(list.isEmpty(), equalTo(false));

    }

}

