/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataServiceTest;

import configuration.Config;
import gq.kiev.homevideoserver.model.HVSDataModel;
import gq.kiev.homevideoserver.service.api.DataService;
import java.util.List;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static utils.Utils.printer;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Config.class)
public class DataServiceTest {
        
    @Autowired
    DataService dataService;
    
    
    
    //private String[] ext = {".mp4"};
    
    
    @Test
    public void findFiles() {
        List<HVSDataModel> dataModels = dataService.getAllHVSDataModels("");
        
        printer(dataModels);
        
        assertThat(dataModels.isEmpty(), equalTo(false));

    }
    
    @Test
    public void getPagesTest() {
        
        
        assertThat( dataService.getAllHVSDataModels("").isEmpty(), equalTo(false));

    }
    

    

}

