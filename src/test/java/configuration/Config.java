package configuration;

import org.junit.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionService;
import org.springframework.format.support.DefaultFormattingConversionService;

@EnableAutoConfiguration
@ComponentScan("gq.kiev.homevideoserver.service")
@Configuration
public class Config {

    @Bean
    public static ConversionService conversionService() {
        return new DefaultFormattingConversionService();
    }

}
