/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configuration;

import gq.kiev.homevideoserver.model.VideoModel;
import gq.kiev.homevideoserver.service.filesystem.FileFinderService;
import java.io.File;
import java.util.List;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static utils.Utils.printer;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Config.class)
public class AppPropertiesTest {


    @Value("${application.video_extension}")
    private List<String> defaultFileExt;

    @Value("${application.exclude_folders}")
    private List<String> defaultExcludeFolders;

    @Value("${application.check_dirs}")
    private Boolean checkDirs;

    @Value("${application.picture.extension}")
    private String pictureExtension;
    
    @Value("${server.port}")
    private Integer serverPort;
    
    @Value("${application.main.path}")
    private String path;
            
    @Value("${application.picture.defaultPicture}")
    private String defaultPicture;
    
    @Test
    public void tesyDefaultPicture() {   
        
        File file = new File(defaultPicture);        
        
        assertThat(file.exists(), equalTo(true));

    }
    
    @Test
    public void testPath() {
        System.out.println("path is : ");
        printer(path);        
        assertThat(path == null, equalTo(false));
    }
    
    @Test
    public void testServerPort() {
        System.out.println("serverPort is : ");
        printer(serverPort);        
        assertThat(serverPort == null, equalTo(false));
    }
    
    @Test
    public void testPictureExtension() {
        System.out.println("PictureExtension is : ");
        printer(pictureExtension);
        
        assertThat(pictureExtension == null || pictureExtension.isEmpty(), equalTo(false));
    }

    @Test
    public void testCheckDirs() {
        System.out.println("Check dirs is : " );
        printer(checkDirs);        
        assertThat(checkDirs == null, equalTo(false));
    }


    @Test
    public void testDefaultFileExt() {
        printer(defaultFileExt);
        assertThat(defaultFileExt.isEmpty(), equalTo(false));
    }

    @Test
    public void testDefaultExcludeFolders() {
        printer(defaultExcludeFolders);
        assertThat(defaultExcludeFolders.isEmpty(), equalTo(false));
    }


}
