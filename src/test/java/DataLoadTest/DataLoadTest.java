/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataLoadTest;

import configuration.Config;
import java.io.File;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Config.class)
public class DataLoadTest {        
    
    @Test
    public void findFiles() {
        
        final String pathToFile = "src/main/resources/static/images/defaultPicture.png";
        
        File file = new File(pathToFile);
        
        
        assertThat(file.exists(), equalTo(true));

    }
}

