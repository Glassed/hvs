/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.List;

/**
 *
 * @author Alexander
 */
public class Utils {
    
    static public void printer(List list) {
        for (Object obj : list) {
            System.out.print("|" + obj + "|");
            System.out.println();
        }
        System.out.println("*******\n*******");
    }

    static public void printer(Object obj) {
        System.out.print("|" + obj + "|");
        System.out.println();
        System.out.println("*******\n*******");
    }
}
