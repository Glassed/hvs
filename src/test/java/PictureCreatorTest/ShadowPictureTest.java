/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PictureCreatorTest;

import configuration.Config;
import gq.kiev.homevideoserver.model.VideoModel;
import gq.kiev.homevideoserver.service.filesystem.FileFinderService;
import gq.kiev.homevideoserver.service.filesystem.ShadowPictureService;
import java.util.List;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static utils.Utils.printer;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Config.class)
public class ShadowPictureTest {

    @Autowired
    FileFinderService finder;
    @Autowired
    ShadowPictureService weakPicture;

    @Test
    public void findFilesAndGetWeakPictureNames() {
        
        finder.refleshFileHolder();
        List<VideoModel> videoModelList = finder.getVideoModels();

        List<String> absolutePicNameList = weakPicture.getShadowPictures(videoModelList);

        printer(absolutePicNameList);
        
        assertThat(videoModelList.isEmpty(), equalTo(false));
        assertThat(absolutePicNameList.isEmpty(), equalTo(false));
    }

}
