var name = "";
var secondsGlobal = 1;
var refreshIntervalId;

google.charts.setOnLoadCallback(function() {
    // set the date we're counting down to
    // variables for time units
    var days, hours, minutes, seconds;

    // get tag element
    var countdown = document.getElementById('countdown');

    retrieveVictim();
    heroes();

    $(document).on("keypress", "#nextInput", function(e) {
        if (e.which == 13) {
            changeVictim();
        }
    });

    // load app tooltips(popups)
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    $('#nextInput').tooltip("hi")

    // update victim
    setInterval(function() {synch()}, 5000);

    // update the tag with id "countdown" every 1 second
});

function dateSpan(days, hours, minutes, seconds) {
    var isD = days > 0;
    var isH = hours > 0;
    var isM = minutes > 0;

    var res = '<span class="seconds">' + seconds + ' <b>Секунд</b></span>';
    if (isD || isH || isM) {
        res = '<span class="minutes">' + minutes + ' <b>Минут</b></span> ' + res;
    }
    if (isD || isH) {
        res = '<span class="hours">' + hours + ' <b>Часов</b></span> ' + res
    }
    if (isD) {
        res = '<span class="days">' + days +  ' <b>Дней</b></span> ' + res;
    }
    return res;
}

function count() {
    // find the amount of "seconds" between now and target
    seconds_left = secondsGlobal;

    // do some time calculations
    days = parseInt(seconds_left / 86400);
    seconds_left = seconds_left % 86400;

    hours = parseInt(seconds_left / 3600);
    seconds_left = seconds_left % 3600;

    minutes = parseInt(seconds_left / 60);
    seconds = parseInt(seconds_left % 60);

    // format countdown string + set tag value
    countdown.innerHTML = dateSpan(days, hours, minutes, seconds);

    secondsGlobal++;
}

function retrieveVictim() {
    $.getJSON("/report/victim", updateVictim);
}

function changeVictim() {
    var next = $("#nextInput").val();
    $.ajax({
        type: "POST",
        url:"/report/victim/change/" + next,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function(data) {
            $("#nextInput").val("");
            $("#nextInput").parent().removeClass("has-warning");
            $("#nextInput").parent().addClass("has-success");
            $("#nextInput").removeAttr('title');
            $('#nextInput').popover('hide')
            updateVictim(data);
        },
        error: function() {
            $("#nextInput").val("");
            $("#nextInput").parent().removeClass("has-success");
            $("#nextInput").parent().addClass("has-warning");
            $("#nextInput").attr('title', 'хуйню вбил, заново!');
            $('#nextInput').popover('show')
        }
    });
}

function updateVictim(data) {
    if (data) {
        name = data.victim;
        var victimField = document.getElementById("victimField");
        secondsGlobal = data.seconds;
        clearInterval(refreshIntervalId);
        count();
        refreshIntervalId = setInterval(function() {count()}, 1000);
        victimField.innerHTML = name + " уже как педрильный : "
        drawChart();
    }
}

function synch() {
    retrieveVictim();
}

function heroes() {
    $.getJSON("/report/heroes", function (data) {
        var heroesString = "";
        $.each(data, function(index, value) {
            heroesString += "<div class='col-xs-3 well'><h3 class='input-text'>" + value.victim + "<br> заработал <br>" + value.seconds + "<br>анальных пробок</h3></div>"
        });
        if (heroesString) {
            $("#heroes").html(heroesString);
        }
    });
}


