
var chart;

google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawChart() {

    chart = new google.visualization.BarChart(document.getElementById('chart_div'));
    // Create the data table.
    var chartData = new google.visualization.DataTable();
    chartData.addColumn('string', 'Names');
    chartData.addColumn('number', 'анальных пробок');

    // Set chart options
    var options = {

        height:300,
        colors:['#e0440e', '#11111', '#ec8f6e', '#f3b49f', '#44444'],
        backgroundColor: '#3B3B3B',
        legendTextStyle:{color: '#E5E5E5'},
        hAxis: {
            textStyle:{color: '#E5E5E5'}
            },
        vAxis: {
            textStyle:{color: '#E5E5E5'}
        }
        };

    updateHeroes(chartData, options);
    setInterval(function () {updateHeroes(chartData, options)}, 5000);
}

function updateHeroes(chartData, options) {
    $.getJSON("/report/heroes", function (data) {
        var chartFullOfData = chartData.getNumberOfRows() > 0;
        $.each(data, function(index, value) {
            if(chartFullOfData) {
                chartData.removeRow(index);
                chartData.insertRows(index, [[value.victim,parseInt(value.seconds)]]);
            } else {
                chartData.addRow([value.victim,parseInt(value.seconds)]);
            }
        });
        chart.draw(chartData, options);
    });
}


