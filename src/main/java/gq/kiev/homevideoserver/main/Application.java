package gq.kiev.homevideoserver.main;

import gq.kiev.homevideoserver.service.dataHolder.DataHolderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.convert.ConversionService;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
@ComponentScan("gq.kiev.homevideoserver")
public class Application extends WebMvcConfigurerAdapter{
    
//    @Value("${application.main.path}")
//    private String path;
    @Autowired
    DataHolderService dataHolder;
    

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
    @Bean
    public static ConversionService conversionService() {
        return new DefaultFormattingConversionService();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/videos/**")
                .addResourceLocations("file:"+dataHolder.getMainPath());
        
    }
}