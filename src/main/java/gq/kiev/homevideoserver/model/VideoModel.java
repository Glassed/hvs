/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.homevideoserver.model;

import java.util.Objects;

/**
 *
 * @author Alexander
 */
public class VideoModel {
    private int id;
    private String name;
    //private String path;
    private String absolutePath;

//    
    public VideoModel(int id, String name, String absolutePath) {
        this.id = id;
        this.name = name;
        //this.path = path;
        this.absolutePath = absolutePath;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public String getPath() {
//        return path;
//    }
//
//    public void setPath(String path) {
//        this.path = path;
//    }
        
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.name);
        //hash = 41 * hash + Objects.hashCode(this.path);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final VideoModel other = (VideoModel) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
//        if (!Objects.equals(this.path, other.path)) {
//            return false;
//        }
        return true;
    }
    
    @Override
    public String toString(){
        String result = "VideoModel " + "id=" + id
                +"\n"
                +"name = "+name
                +"\n"
                +"absolute path = "+absolutePath;
        return result;
    }
    
}
