/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.homevideoserver.model;

import java.util.Objects;

/**
 *
 * @author Alexander
 */
public class HVSDataModel {
    private final String shadowPicture;
    private final String defaultPicture;
    private final VideoModel videoModel;
    private final int id;
        
    public HVSDataModel(String shadowPicture, String defaultPicture, VideoModel videoModel) {
        this.shadowPicture = shadowPicture;
        this.defaultPicture = defaultPicture;
        this.videoModel = videoModel;
        this.id = videoModel.getId();
    }

    public String getDefaultPicture() {
        return defaultPicture;
    }
        
    public int getId(){
        return this.id;
    }
    
    public String getShadowPicture() {
        return shadowPicture;
    }

    public VideoModel getVideoModel() {
        return videoModel;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.shadowPicture);
        hash = 29 * hash + Objects.hashCode(this.videoModel);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HVSDataModel other = (HVSDataModel) obj;
        if (!(this.shadowPicture.equals(other.shadowPicture))) {
            return false;
        }
        if (!(this.videoModel.equals(other.videoModel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        String result = "\nVIDEO :\n"
                + videoModel.getAbsolutePath()
                +"\nPICTURE :\n"
                + shadowPicture
                +"\nPICTURE :\n"
                + defaultPicture
                +"\n===========";
        return result;
    }
}
