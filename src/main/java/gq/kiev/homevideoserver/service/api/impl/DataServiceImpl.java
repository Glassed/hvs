/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.homevideoserver.service.api.impl;

import gq.kiev.homevideoserver.service.api.*;
import gq.kiev.homevideoserver.model.HVSDataModel;
import gq.kiev.homevideoserver.model.VideoModel;
import gq.kiev.homevideoserver.service.dataHolder.DataHolderService;
import gq.kiev.homevideoserver.service.filesystem.FileFinderService;
import gq.kiev.homevideoserver.service.filesystem.ImageCreatorService;
import gq.kiev.homevideoserver.service.filesystem.ShadowPictureService;
import gq.kiev.homevideoserver.service.searcher.SearcherService;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alexander
 */
@Service
public class DataServiceImpl implements DataService {

    @Autowired
    DataServiceImpl(FileFinderService finder) {
        this.finder = finder;
        this.finder.refleshFileHolder();
    }

    @Autowired
    DataHolderService dataHolder;

    @Autowired
    SearcherService searcherService;

    @Value("${application.picture.defaultPicture}")
    private String defaultPicture;

    private FileFinderService finder;

    @Autowired
    private ShadowPictureService shadowPictureService;

    @Autowired
    private ImageCreatorService imageCreator;

    private final int numberOfItemsPerPage = 10;

    @Override
    public List<HVSDataModel> getAllHVSDataModels(String matcher) {
        List<HVSDataModel> result = new ArrayList<>();
        for (VideoModel videoModel : finder.getVideoModels()) {
            //we need this for browser:
            videoModel.setAbsolutePath(correctifyPath(videoModel.getAbsolutePath()));

            String shadowPictureName
                    = shadowPictureService.getShadowPicture(videoModel
                            .getAbsolutePath());

            HVSDataModel preResult = new HVSDataModel(shadowPictureName, defaultPicture, videoModel);

            result.add(preResult);
        }

        return searcherService.searchVideoName(matcher, result);
    }

    @Override
    public TreeMap<Integer, List<HVSDataModel>> getSortedPages(String matcher) {
        TreeMap<Integer, List<HVSDataModel>> result = new TreeMap<>();
        for (int i = 1; i <= getNumberOfPages(matcher); i++) {
            result.put(i, getPage(i, matcher));
        }
        return result;
    }
   

    private List<HVSDataModel> getPage(int number, String matcher) {
        //if number is wrong:
        if (number > getNumberOfPages(matcher) || number < 1) {
            return new ArrayList<HVSDataModel>();
        }
        //define last item id in page:
        int to = number * numberOfItemsPerPage;

        if (to > getAllHVSDataModels(matcher).size()) {
            to = getAllHVSDataModels(matcher).size();
        }
        int from = number * numberOfItemsPerPage - numberOfItemsPerPage;
        return getAllHVSDataModels(matcher).subList(from, to);

    }

    private int getNumberOfPages(String matcher) {

        int numberOfItems = getAllHVSDataModels(matcher).size();
        if (numberOfItems == 0) {
            return 1;
        }
        int numberOfPages = numberOfItems / numberOfItemsPerPage;
        int backwardMiltiply = numberOfPages * numberOfItemsPerPage;

        if (backwardMiltiply == numberOfItems) {
            return numberOfPages;
        }
        //return will be not less than 1;
        return numberOfPages + 1;

    }

    private String correctifyPath(String absoluteName) {

        String fileName = absoluteName.substring(dataHolder.getMainPath().length());//path.length());

        fileName = fileName.replace("\\", "/");

        StringBuilder builder = new StringBuilder(fileName);
        String suffix = "../videos/";
        builder.insert(0, suffix);

        return builder.toString();
    }
    
    @Override
    public String getMainDir() {
        return dataHolder.getMainPath();
    }

    @Override
    public void refleshVideoFileHolder() {
        finder.refleshFileHolder();
    }

    @Override
    public void createPicturesForVideoFiles() {
        imageCreator.createImages(finder.getAbsoluteVideoNames());
    }
    
    @Override
    public boolean changeMainPath(String path) {

        boolean result = dataHolder.setMainPath(path);
        //if path is OK (IS EXIST AND IS DIR)
        if (result) {
            refleshVideoFileHolder();
            return true;
        }
        return false;
    }

}

//    private List<List<HVSDataModel>> getPages(String matcher) {
//        List<List<HVSDataModel>> result = new ArrayList<>();
//        for (int i = 1; i <= getNumberOfPages(matcher); i++) {
//            result.add(getPage(i, matcher));
//        }
//        return result;
//    }
