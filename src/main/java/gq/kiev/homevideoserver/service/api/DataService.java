/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.homevideoserver.service.api;

import gq.kiev.homevideoserver.model.HVSDataModel;
import java.util.List;
import java.util.TreeMap;

/**
 *
 * @author Alexander
 */
public interface DataService {
    
    List<HVSDataModel> getAllHVSDataModels(String matcher);
    void refleshVideoFileHolder();
    void createPicturesForVideoFiles();
    TreeMap<Integer, List<HVSDataModel>> getSortedPages(String matcher);
    @Deprecated
    boolean changeMainPath(String path);
    String getMainDir();
}
