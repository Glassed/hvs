/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.homevideoserver.service.dataHolder.impl;

import gq.kiev.homevideoserver.service.dataHolder.DataHolderService;
import java.io.File;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alexander
 */
@Service
public class DataHolderServiceImpl implements DataHolderService{
    
    @Value("${application.main.path}")
    private String path;
    
    @Override
    public String getMainPath() {
        return path;
    }

    @Override
    public boolean setMainPath(String path) {
        File dir = new File(path);
        if(dir.isDirectory()){
            this.path=path;
            return true;
        }
        return false;
    }
    
}
