/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.homevideoserver.service.dataHolder;

/**
 *
 * @author Alexander
 */
public interface DataHolderService {
    String getMainPath();
    boolean setMainPath(String path);
}
