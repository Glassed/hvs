/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.homevideoserver.service.searcher.impl;

import gq.kiev.homevideoserver.model.HVSDataModel;
import gq.kiev.homevideoserver.service.searcher.SearcherService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alexander
 */
@Service
public class SearcherServiceImpl implements SearcherService{

    @Override
    public List<HVSDataModel> searchVideoName(String matcher, List<HVSDataModel>list) {
        if(matcher==null||matcher.isEmpty()){
            return list;
        }
        
        matcher = matcher.toLowerCase();
        
        List<HVSDataModel>  result = new ArrayList<>();
        for(HVSDataModel hvsDataModel:list){
            String videoName = hvsDataModel.getVideoModel().getName().toLowerCase();
            if(videoName.contains(matcher)){
                result.add(hvsDataModel);
            }
        }
        return result;
         
    }
    
}
