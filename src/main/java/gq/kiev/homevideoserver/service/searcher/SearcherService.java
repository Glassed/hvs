/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.homevideoserver.service.searcher;

import gq.kiev.homevideoserver.model.HVSDataModel;
import java.util.List;

/**
 *
 * @author Alexander
 */
public interface SearcherService {
    List<HVSDataModel> searchVideoName(String matcher, List<HVSDataModel>list);
}
