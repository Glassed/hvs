/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.homevideoserver.service.filesystem;

import gq.kiev.homevideoserver.model.VideoModel;
import java.io.File;
import java.util.List;

/**
 *
 * @author Alexander
 */
public interface ShadowPictureService {    
    List<String> getShadowPictures(List<VideoModel> list);
    String getShadowPicture(String videoName);
}
