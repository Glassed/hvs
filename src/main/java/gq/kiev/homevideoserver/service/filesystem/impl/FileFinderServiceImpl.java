package gq.kiev.homevideoserver.service.filesystem.impl;

import gq.kiev.homevideoserver.model.VideoModel;
import gq.kiev.homevideoserver.service.dataHolder.DataHolderService;
import gq.kiev.homevideoserver.service.filesystem.FileFinderService;
import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alexander
 */
@Service
public class FileFinderServiceImpl implements FileFinderService {

    @Value("${application.video_extension}")
    private List<String> defaultFileExt;

    @Value("${application.exclude_folders}")
    private List<String> excludeFolders;

    @Value("${application.check_dirs}")
    private boolean checkDirs;
    
    @Autowired
    DataHolderService dataHolder;
    
    private static List<File> collectedVideos;
    private final List<File> correctDirsAndVideos;

    public FileFinderServiceImpl(){
        collectedVideos = new ArrayList<>();
        correctDirsAndVideos = new ArrayList<>();
    }

    @Override
    public synchronized void refleshFileHolder(String pathName) {
        collectedVideos = fileSystemRat(pathName);
    }

    @Override
    public synchronized void refleshFileHolder() {
        collectedVideos = fileSystemRat(dataHolder.getMainPath());
    }

    @Override
    public List<String> getAbsoluteVideoNames() {
        List<String> result = new ArrayList<>();
        if (collectedVideos == null) {
            return result;
        }
        for(File collectedVideo : collectedVideos){
            result.add(collectedVideo.getAbsolutePath());
        }
        return result;
    }

    @Override
    public List<VideoModel> getVideoModels() {
        List<VideoModel> result = new ArrayList<>();
        int id = 0;

        for (File file : collectedVideos) {
            String name = correctifyName(file);
            //String path = correctifyPath(file);
            String absolutePath = file.getAbsolutePath();
            VideoModel videoModel = new VideoModel(id++, name, absolutePath);//path, 
            result.add(videoModel);
        }
        return result;
    }

    private String correctifyName(File file) {
        String name = file.getName();
        int lastPointIndex = name.lastIndexOf(".");
        return name.substring(0, lastPointIndex);
    }

//    private String correctifyPath(File file) {
//
//        String fileName = file
//                .getAbsolutePath().substring(path.length());
//
//        fileName = fileName.replace("\\", "/");
//
//        StringBuilder builder = new StringBuilder(fileName);
//        String suffix = "../videos/";
//        builder.insert(0, suffix);
//
//        return builder.toString();
//    }

    private List<File> fileSystemRat(String pathName) {
        if (!dirNameIsOk(pathName)) {
            System.out.println("Directory is not OK! ;)");
            return new ArrayList<>();
        }
        correctDirsAndVideos.clear();
        return getFiles(new File(pathName));
    }

    private static boolean dirNameIsOk(String pathName) {
        try {
            File dir = new File(pathName);
            if (!dir.exists()) {
                return false;
            }
            if (dir.isFile()) {
                return false;
            }
        } catch (NullPointerException npe) {
            return false;
        }
        return true;
    }

    private List<File> getFiles(File dir) {

        for (File file : dir.listFiles(fileFilter)) {
            try {
                if (file.isDirectory()) {
                    getFiles(file);
                } else {
                    correctDirsAndVideos.add(file);
                }
            } catch (NullPointerException npe) {
                System.out.println("**********************");
                System.out.println("npe at file " + file.getName());
                System.out.println("**********************");
            }

        }
        return correctDirsAndVideos;
    }

    //My own FileFilter:
    private final FileFilter fileFilter = new FileFilter() {
        @Override
        public boolean accept(File filename) {
            if (filename.isDirectory()) {
                return chkDir(filename);
            } else {
                return chkFile(filename);
            }
        }

        private boolean chkFile(File filename) {
            for (String fileExt : defaultFileExt) {
                if (filename.getName().endsWith(fileExt)) {
                    return true;
                }
            }
            return false;
        }

        private boolean chkDir(File filename) {
            for (String excFolderName : excludeFolders) {
                if (filename.getName().equalsIgnoreCase(excFolderName)) {
                    return false;
                }
            }
            return checkDirs;
        }
    };

}
