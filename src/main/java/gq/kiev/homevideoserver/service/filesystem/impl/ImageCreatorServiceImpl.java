/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.homevideoserver.service.filesystem.impl;

import gq.kiev.homevideoserver.model.VideoModel;
import gq.kiev.homevideoserver.service.filesystem.*;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.jcodec.api.JCodecException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.imageio.ImageIO;
import org.jcodec.api.FrameGrab;
import org.jcodec.common.model.ColorSpace;
import org.jcodec.common.model.Picture;
import org.jcodec.scale.AWTUtil;
import org.jcodec.scale.ColorUtil;
import org.jcodec.scale.Transform;
import org.springframework.beans.factory.annotation.Value;

/**
 *
 * @author Alexander
 */
@Service
public class ImageCreatorServiceImpl implements ImageCreatorService {

    @Autowired
    ShadowPictureService pictureService;

    private final static int DEF_FRAME_NUMBER = 9000;

    @Value("${application.picture.extension}")
    private String pictureExtension;

    @Override
    public void createImages(List<String> videoName) {
        createImages(videoName, DEF_FRAME_NUMBER);
    }

    @Override
    public void createImages(List<String> videoNameList, int frameNumber) {
        for (String videoName : videoNameList) {
            createPicture(videoName, frameNumber);
        }
    }

    private void createPicture(String videoName, int frameNumber) {
        try {
            create(videoName, frameNumber);
        } catch (IOException ex) {
            messagePrinter("Э", videoName, "CAUSED IOException!");
        } catch (JCodecException ex) {
            messagePrinter("Э", videoName, "CAUSED JCodecException!");
        } catch (ArrayStoreException ex) {
            messagePrinter("Э", videoName, "CAUSED ArrayStoreException!");
        } catch (NullPointerException ex){
            messagePrinter("Э", videoName, "CAUSED NullPointerException!");
        }

    }

    private void create(String videoName, int frameNumber) throws IOException, JCodecException {
        Picture frame = null;
        String absPictureName = pictureService.getShadowPicture(videoName);
        File fileToCreate = new File(absPictureName);
        if (fileToCreate.exists()) {
            //need message:
            messagePrinter("^", fileToCreate.getAbsolutePath(), "already exists");
            return;
        }

        frame = FrameGrab.getNativeFrame(new File(videoName), frameNumber);

        Transform transform = ColorUtil.getTransform(frame.getColor(), ColorSpace.RGB);
        Picture rgb = Picture.create(frame.getWidth(), frame.getHeight(), ColorSpace.RGB);
        transform.transform(frame, rgb);
        BufferedImage bi = AWTUtil.toBufferedImage(rgb);

        ImageIO.write(bi, pictureExtension.substring(1), fileToCreate);
        
        //need message:
        messagePrinter("&", absPictureName, "created");
    }
    
    private void messagePrinter(String prefix, Object arg, String message){
        String result = prefix+" "+arg.toString()+" "+message;
        System.out.println(result);
    }
}
