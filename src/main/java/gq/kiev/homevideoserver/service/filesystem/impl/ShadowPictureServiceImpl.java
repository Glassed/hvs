/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.homevideoserver.service.filesystem.impl;

import gq.kiev.homevideoserver.model.VideoModel;
import gq.kiev.homevideoserver.service.filesystem.ShadowPictureService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alexander
 */
@Service
public class ShadowPictureServiceImpl implements ShadowPictureService {

    @Value("${application.picture.extension}")
    private String pictureExtension;

    @Value("${application.video_extension}")
    private String videoExtension;

    @Override
    public List<String> getShadowPictures(List<VideoModel> list) {
        List<String> result = new ArrayList<>();
        for (VideoModel videoModel : list) {
            if (videoSourceOk(videoModel.getAbsolutePath())) {
                String absPicName = createAbsolutePictureName(videoModel.getAbsolutePath());
                result.add(absPicName);
            }
        }
        return result;
    }

    @Override
    public String getShadowPicture(String videoName) {
        if (videoSourceOk(videoName)) {
            return createAbsolutePictureName(videoName);
        }
        else throw new IllegalArgumentException("WRONG VIDEO FILE!\n"+videoName);
    }

//
//    @Override
//    public List<PictureModel> getPictureModels(List<VideoModel> list) {
//        List<PictureModel> result = new ArrayList<>();
//        for (VideoModel videoModel : list) {
//            int id = videoModel.getId();
//            String picName = videoModel.getName();
//            String absPicName = createAbsolutePictureName(videoModel);
//            result.add(new PictureModel(id, picName, absPicName));
//        }
//        return result;
////    }
//
//    @Override
//    public PictureModel getPictureModel(VideoModel videoModel) {
//        int id = videoModel.getId();
//        String picName = videoModel.getName();
//        String absPicName = createAbsolutePictureName(videoModel);
//        return new PictureModel(id, picName, absPicName);
//    }
    private String createAbsolutePictureName(String absoluteVideoName) {
        StringBuilder builder = new StringBuilder(absoluteVideoName);
        int startIndex = builder.lastIndexOf(".");
        int endIndex = builder.length();
        builder = builder.replace(startIndex, endIndex, pictureExtension);
        return builder.toString();
    }

    private boolean videoSourceOk(String absoluteVideoName) {
        String fullMovieName = absoluteVideoName;
        return videoExtension
                .equalsIgnoreCase(getFileExtension(fullMovieName));

    }

    private String getFileExtension(String fullMovieName) {
        int lastPointAt = fullMovieName.lastIndexOf(".");
        return fullMovieName.substring(lastPointAt);
    }

}
