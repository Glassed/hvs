/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.homevideoserver.service.filesystem
;

import gq.kiev.homevideoserver.model.VideoModel;
import java.io.File;
import java.util.List;

/**
 *
 * @author Alexander
 */
public interface FileFinderService {
    /**
     * Находит все абсолютные имена файлов что были в заданной файлом application.properties
     * директории, с заданным файлом application.properties расширением.
     * Вложенные директории будут проверяться, если флаг 
     * checkdirs=true в application.properties
     * @return 
     */
    List<String> getAbsoluteVideoNames();
    
    /**
     * Находит все файлы что были в заданной файлом application.properties
     * директории, с заданным файлом application.properties расширением.
     * Вложенные директории будут проверяться, если флаг 
     * application.checkdirs=true в application.properties И формирует из них коллекцию VideoModel 
     * обьектов.
     * @return 
     */
    List<VideoModel> getVideoModels();
    
    
    void refleshFileHolder(String pathName);
    void refleshFileHolder();
}
