package gq.kiev.homevideoserver.controllers;

import gq.kiev.homevideoserver.model.HVSDataModel;
import gq.kiev.homevideoserver.service.api.DataService;
import java.util.TreeSet;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {

    @Autowired
    private DataService dataService;

    @RequestMapping("/")
    public String simpleHello(Model model) {
        final int firstPage = 1;
        model.addAttribute("pageData", dataService.getSortedPages("").get(firstPage));
        TreeSet<Integer> pageNumberList = new TreeSet<>(dataService.getSortedPages("").keySet());
        model.addAttribute("pageNumberList", pageNumberList);
        return "listVideo";
    }

    @RequestMapping(value = "search/", method = RequestMethod.POST)
    public String simpleSearch(Model model, HttpServletRequest request,
            @RequestParam(value = "matcher", required = true) String matcher) {
        final int firstPage = 1;

        model.addAttribute("currentMatcher", matcher);
        model.addAttribute("pageData", dataService.getSortedPages(matcher).get(firstPage));
        TreeSet<Integer> pageNumberList = new TreeSet<>(dataService.getSortedPages(matcher).keySet());
        model.addAttribute("pageNumberList", pageNumberList);
        return "listVideo";
    }

//    @RequestMapping(value = "/", method = RequestMethod.POST)
//    public ModelAndView changeDir(HttpServletRequest request,
//            @RequestParam(value = "newPath", required = true) String path) {
//        dataService.changeMainPath(path);
//        return new ModelAndView("redirect:/");
//    }
    //Video showing window;
    @RequestMapping(value = "video/{id}", method = RequestMethod.GET)
    public String videoById(@PathVariable Integer id, Model model) {
        HVSDataModel hvsDataModel = null;
        try {
            hvsDataModel = dataService.getAllHVSDataModels("").get(id);
        } catch (IndexOutOfBoundsException ex) {
            System.err.println("Video showing window cannot find needed file");
        }

        model.addAttribute("hvsDataModel", hvsDataModel);
        return "singleVideo";
    }

    @RequestMapping(value = "page/{id}", method = RequestMethod.GET)
    public String pageById(@PathVariable Integer id, Model model) {
        final int currentPage = id;
        model.addAttribute("pageData", dataService.getSortedPages("").get(currentPage));

        TreeSet<Integer> pageNumberList = new TreeSet<>(dataService.getSortedPages("").keySet());
        model.addAttribute("pageNumberList", pageNumberList);
        return "listVideo";

    }

    @RequestMapping("/find")
    public ModelAndView findAllFiles() {
        dataService.refleshVideoFileHolder();
        return new ModelAndView("redirect:/");
    }

    @RequestMapping("/renewPictures")
    public ModelAndView renewPictures() {
        dataService.createPicturesForVideoFiles();
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(
            @RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout) {

        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Invalid username and password!");
        }

        if (logout != null) {
            model.addObject("msg", "You've been logged out successfully.");
        }
        model.setViewName("loginpage");

        return model;

    }

    @RequestMapping("favicon.ico")
    public String favicon() {
        return "forward:images/favicon.ico";
    }
}
